Concepts:

Smoke test - for example root_path leads to implementing routes, controller and view, making full flow work

`rails g` -> shows available rails generator commands
Model.save vs Model.create = save used in forms. Returns nil if validations fail. Create returns object. and saves directly

touch

redirect_to vs render -> redirect loads route, render will jsut render the view (unless passed options)

whitelisting params

Routes:
resources vs resource -> resource, is used when there is no need to specifiy id in url, e.g a session, or when only one user at time



Migrations:
migrations define the schema. Final schema is used ot generate db, instead of rerunning all migrations, because of error possibs

Features:

TDD, leads to creating components with classes which can be easily referenced in tests

feature(exampleGroup, subclass) and scenario(Example, instance), correspond to describe and it.

session is avaialable to us in controllers, can addwhatever. usually frmoparams

Rspec config:
config.include Features, type: :feature     --> only include in tagged example instances

Active Record:
- e.g Todo.where(email: ['hello@mail.com, yo@mail.com']) can pass in array
- ActiveRecord::ColelctionProxy (array) - can receive create method, returns array with object added


Database Cleaner:
- transaction, deletion, truncation
- 

present?
