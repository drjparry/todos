require 'rails_helper'

feature 'User completes todo' do
  scenario 'successfully' do
    sign_in_as('john@smith.com')
    create_todo('David rocks')

    expect(page).to display_todo("David rocks")
    click_on "Mark complete"
    expect(page).to display_completed_todo("David rocks")
  end
end
