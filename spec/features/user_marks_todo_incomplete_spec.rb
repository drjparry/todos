require 'rails_helper'

feature 'User marks todo incomplete' do
  scenario 'successfully' do
    sign_in_as('john@smith.com')
    create_todo('David rocks')

    expect(page).to display_todo("David rocks")

    click_on "Mark complete"
    click_on "Mark incomplete"

    expect(page).not_to display_completed_todo("David rocks")
    expect(page).to display_todo("David rocks")
  end
end

