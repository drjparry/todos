require 'rails_helper'

feature 'User sees own todo' do
  scenario 'does not see others todos' do
    Todo.create!(title: "Buy milk", email: 'trhuckh@no.never')

    sign_in_as('peter@hotmail.com')

    expect(page).not_to display_todo("David rocks")
  end
end
