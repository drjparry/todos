require 'rails_helper'

feature 'User creates todo' do
  scenario 'successfully' do
    sign_in

    create_todo('David rocks')

    expect(page).to display_todo("David rocks")
  end
end
